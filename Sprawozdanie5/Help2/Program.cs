﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Help2
{
    class Program
    {
        static void Main(string[] args)
        {
            const float pi = 3.1415f;
            const int N = 9;
            float[][] Corners = new float[N][];
            for (int i = 0; i < Corners.GetLength(0); i++)
            {
                Corners[i] = new float[3];
            }
            for (int i = 0; i < N; ++i)
            {
                float kat = (float)i / N * pi * 2.0f;
                float x = (float)Math.Sin(kat);
                float y = (float)Math.Cos(kat);
                Corners[i][0] = x;
                Corners[i][1] = 0.0f;
                Corners[i][2] = y;
            }
        }
    }
}
