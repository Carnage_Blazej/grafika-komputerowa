// Zad2.cpp : Narysowa� w oknie programu Program1 wielok�t. Dodatkowo uzyska� mo�liwo�� zmiany po�o�enia i rozmiaru okna, tak �eby kod renderuj�cy odpowiednio reagowa� na te zmiany.
//
#include "stdafx.h"
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

GLfloat startx = 165.0f;
GLfloat starty = 125.0f;

GLfloat windowWidth;
GLfloat windowHeight;

void RenderScene(void) {
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(255.0f, 255.0f, 255.0f);

	glBegin(GL_POLYGON);
	glVertex2f(0 + startx, 20 + starty);
	glVertex2f(12.8558 + startx, 15.3209 + starty);
	glVertex2f(19.6962 + startx, 3.47295 + starty);
	glVertex2f(17.3205 + startx, -10 + starty);
	glVertex2f(6.8404 + startx, -18.7939 + starty);
	glVertex2f(-6.8404 + startx, -18.7939 + starty);
	glVertex2f(-17.3205 + startx, -10 + starty);
	glVertex2f(-19.6962 + startx, 3.47296 + starty);
	glVertex2f(-12.8558 + startx, 15.3209 + starty);
	glEnd();

	glutSwapBuffers();
}

void SetupRC(void) {   
	glClearColor(150 / 255.0f, 75 / 255.0f, 0.0f, 0.0f);
}

void ChangeSize(GLsizei w, GLsizei h) {
	if (h == 0)
		h = 1;

	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (w <= h)
	{
		windowHeight = 250.0f*h / w;
		windowWidth = 250.0f;
	}
	else
	{
		windowWidth = 250.0f*w / h;
		windowHeight = 250.0f;
	}

	glOrtho(0.0f, windowWidth, 0.0f, windowHeight, 1.0f, -1.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void main(int argc, char* argv[]) {
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);	
	glutInitWindowSize(800, 600);					
	glutCreateWindow("M�j pierwszy program w GLUT");

	glutDisplayFunc(RenderScene); 
	glutReshapeFunc(ChangeSize);  

	SetupRC();
	glutMainLoop();
}
