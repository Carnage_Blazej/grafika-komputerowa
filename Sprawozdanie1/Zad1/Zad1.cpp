// Zad1.cpp : Opracowa� program kt�ry tworzy w GLUT standardowe okno interfejsu GUI z nag��wkiem "M�j pierwszy program w GLUT"  i pustym br�zowym t�em.
//
#include "stdafx.h"
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

void RenderScene(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	glutSwapBuffers();
}

void SetupRC(void) {   
	glClearColor(150 / 255.0f, 75 / 255.0f, 0.0f, 0.0f);
}

void main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);	
	glutInitWindowSize(800, 600);					
	glutCreateWindow("M�j pierwszy program w GLUT");
	glutDisplayFunc(RenderScene);
	SetupRC();
	glutMainLoop();
}