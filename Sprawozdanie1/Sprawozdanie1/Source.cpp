#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

// Pocztkowy rozmiar i pozycja prostokta
GLfloat startx = 165.0f;
GLfloat starty = 125.0f;
GLfloat x1 = 1.0f;
GLfloat y1 = 1.0f;
GLfloat rxsize = 39.3924;
GLfloat rysize = 38.7939;

// Rozmiar kroku (liczba pikseli) w osi x i y
GLfloat xstep = 0.03f;
GLfloat xpstep = 1.0f;
GLfloat ypstep = 1.0f;
// Dane zmieniajcych si� rozmiar�w okna
GLfloat windowWidth;
GLfloat windowHeight;
///////////////////////////////////////////////////////////
// Wywo�ywana w celu przerysowania sceny
void RenderScene(void) {
	// Wyczyszczenie okna aktualnym kolorem czyszcz�cym
	glClear(GL_COLOR_BUFFER_BIT);

	// Aktualny kolor rysuj�cy - czerwony
	//	R	G	B
	glColor3f(255.0f, 255.0f, 255.0f);

	// Narysowanie prostok�ta wype�nionego aktualnym kolorem
	glBegin(GL_POLYGON);
		glVertex2f(0 + startx, 20 + starty);
		glVertex2f(12.8558*x1 + startx, 15.3209 + starty);
		glVertex2f(19.6962*x1 + startx, 3.47295 + starty);
		glVertex2f(17.3205*x1 + startx, -10 + starty);
		glVertex2f(6.8404*x1 + startx, -18.7939 + starty);
		glVertex2f(-6.8404*x1 + startx, -18.7939 + starty);
		glVertex2f(-17.3205*x1 + startx, -10 + starty);
		glVertex2f(-19.6962*x1 + startx, 3.47296 + starty);
		glVertex2f(-12.8558*x1 + startx, 15.3209 + starty);
	glEnd();

	// Wys�anie polece� do wykonania - !!! dla animacji to jest inne polecenie
	glutSwapBuffers();
}
///////////////////////////////////////////////////////////
// Wywo�ywana przez bibliotek GLUT w czasie, gdy okno nie
// jest przesuwane ani nie jest zmieniana jego wielko��
void Przesuwanie(int value) {
	// Odwr�cenie kierunku, je�eli osi�gni�to lew� lub praw� kraw�d�
	if (startx > windowWidth - (rxsize/2) || startx < rxsize/2)
		xpstep = -xpstep;

	// Odwr�cenie kierunku, je�eli osi�gni�to doln� lub g�rn� kraw�d�
	if (starty > windowHeight - (rysize/2) || starty < rysize/2)
		ypstep = -ypstep;


	// Kontrola obramowania. Wykonywana jest na wypadek, gdyby okno     
	// zmniejszy�o swoj wielko�� w czasie, gdy kwadrat odbija� si� od     
	// kraw�dzi, co mog�oby spowodowa�, �e znalaz� by si� poza      
	// przestrzeni� ograniczajc�.     
	if (x1 > windowWidth - rxsize) 
		x1 = windowWidth - rxsize - 1;

	if (y1 > windowHeight - rysize)
		y1 = windowHeight - rysize - 1;

	// Wykonanie przesuni�cia kwadratu
	startx += xpstep;
	starty += ypstep;

	// Ponowne rysowanie sceny z nowymi wsp�rz�dnymi  
	glutPostRedisplay();
	glutTimerFunc(33, Przesuwanie, 1);
}

void Obracanie(int value) {
	if (x1 > 1 || x1 < -1)
	{
		xstep = -xstep;
	}

	x1 += xstep;

	glutPostRedisplay();
	glutTimerFunc(33, Obracanie, 1);
}
///////////////////////////////////////////////////////////
// Konfigurowanie stanu renderowania
void SetupRC(void) {
	// Ustalenie niebieskiego koloru czyszcz�cego     
	glClearColor(150/255.0f, 75/255.0f, 0.0f, 0.0f);
}
///////////////////////////////////////////////////////////
// Wywo�ywana przez bibliotek GLUT przy ka�dej zmianie wielko�ci okna
void ChangeSize(GLsizei w, GLsizei h) {
	// Zabezpieczenie przed dzieleniem przez zero
	if (h == 0)
		h = 1;

	// Ustalenie wielko�ci widoku zgodnego z rozmiarami okna
	glViewport(0, 0, w, h);

	// Ustalenie uk�adu wsp�rz�dnych
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Keep the square square, this time, save calculated
	// width and height for later use
	if (w <= h)
	{
		windowHeight = 250.0f*h / w;
		windowWidth = 250.0f;
	}
	else
	{
		windowWidth = 250.0f*w / h;
		windowHeight = 250.0f;
	}

	// Ustanowienie przestrzeni ograniczaj�cej (lewo, prawo, d�, g�ra, blisko, daleko)
	glOrtho(0.0f, windowWidth, 0.0f, windowHeight, 1.0f, -1.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
///////////////////////////////////////////////////////////
// G��wny punkt wej�cia programu
void main(int argc, char* argv[]) {
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);	//
	glutInitWindowSize(800, 600);					//
	glutCreateWindow("M�j pierwszy program w GLUT");//ZAD 1

	glutDisplayFunc(RenderScene); //
	glutReshapeFunc(ChangeSize);  //ZAD 2

	//glutTimerFunc(33, Przesuwanie, 1); //ZAD 3
	glutTimerFunc(33, Obracanie, 1); //ZAD 4
	// OBRACANIE/PRZESUWANIE MOZNA WLACZAC/WYLACZAC ZAKOMENTOWUJAC POWYZSZE LINIE WYWOLUJACE METODY

	SetupRC();
	glutMainLoop();
}
